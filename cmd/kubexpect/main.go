package main

import (
	"log"
	"path"

	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/clientcmd"
)

func main() {
	pods := getPods(path.Join("/home/zephinzer/.kube/config"))
	for i := 0; i < len(pods); i++ {
		item := pods[i]
		log.Println(item.ObjectMeta.Namespace, item.ObjectMeta.Name)
	}

	deployments := getDeployments(path.Join("/home/zephinzer/.kube/config"))
	for i := 0; i < len(deployments); i++ {
		item := deployments[i]
		log.Println(item.ObjectMeta.Namespace, item.ObjectMeta.Name)
	}
}

func getDeployments(kubeconfigPath string) []appsv1.Deployment {
	config, _ := clientcmd.BuildConfigFromFlags("", kubeconfigPath)
	client, _ := kubernetes.NewForConfig(config)
	deployments, _ := client.AppsV1().Deployments("").List(metav1.ListOptions{})
	return deployments.Items
}

func getPods(kubeconfigPath string) []corev1.Pod {
	config, _ := clientcmd.BuildConfigFromFlags("", kubeconfigPath)
	client, _ := kubernetes.NewForConfig(config)
	pods, _ := client.CoreV1().Pods("").List(metav1.ListOptions{})
	return pods.Items
}

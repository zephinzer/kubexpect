package config

type ResourceType string

const (
	Pod                   ResourceType = "pod"
	Deployment            ResourceType = "deployment"
	Service               ResourceType = "service"
	Ingress               ResourceType = "ingress"
	ConfigMap             ResourceType = "configmap"
	Secret                ResourceType = "secret"
	PersistentVolume      ResourceType = "persistentvolume"
	PersistentVolumeClaim ResourceType = "persistentvolumeclaim"
)

type Config struct {
	Resource ResourceType
	Name     string
	Replicas uint
}

func Initialize()

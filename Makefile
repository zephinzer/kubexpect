IMAGE_PATH=zephinzer/kubexpect

bin:
	CGO_ENABLED=0 go build -v -a -ldflags "-s -w -extldflags 'static'" -o ./bin/kubexpect ./cmd/kubexpect
	upx ./bin/kubexpect -o ./bin/kubexpect_upxed

build/image:
	docker build --file ./build/Dockerfile --tag $(IMAGE_PATH):latest .
	mkdir -p ./build/image
	docker save $(IMAGE_PATH):latest -o ./build/image/latest.tar.gz

# Kubexpect

Also available as a `kubectl` plugin (`kubectl-expect`).


- - -


# Usage

```sh
kubectl expect deployment $DEPLOYMENT_NAME --replicas 3 --state READY
```


- - -


# Development

## Required Software

1. Go
2. Make
3. UPX


- - -


# License

Code is licensed under the MIT license.
